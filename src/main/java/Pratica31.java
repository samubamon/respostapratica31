
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author samu
 */
public class Pratica31 {
        private static Date inicio, fim, dataAtual;
        private static String meuNome;
        private static GregorianCalendar dataNascimento;
        private static long dif;//dif é a diferença em milissegundos entre dataNascimento e dataAtual.
        
        
    public static void main(String[] args){
        inicio = new Date();
        
        //Item 4.
        meuNome = "Samuel Roque de Miranda Bamonte";
        //Item 5.
        System.out.println(meuNome.toUpperCase());
        //Item 6.
        System.out.println(meuNome.substring(24,31)+","+meuNome.charAt(0)+"."+meuNome.charAt(7)+"."+meuNome.charAt(16)+".");
        //Item 7.
        dataNascimento = new GregorianCalendar(1997, Calendar.APRIL, 21);
        dataAtual = new Date();
        
        dif = dataAtual.getTime() - dataNascimento.getTime().getTime();
        dif /= (1000*60*60*24);//Convertendo milisegundos em dias.
        System.out.println(dif + " dias");
        
        fim = new Date();
        
        //Por fim, calculamos o tempo decorrido de processamento entre os item 4 e 7, em milisegundos
        System.out.println(fim.getTime() - inicio.getTime() + " milisegundos");
       
        
        
        
    }
        
        
        

    
}
